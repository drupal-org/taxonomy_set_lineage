<?php

/**
 * @file
 * Taxonomy Set Lineage makes sure that parent terms are saved in the database.
 *
 * When an entity is saved, the module will check if the vocabulary has Save
 * Lineage enabled.  If changes to the terms have been made, the module will
 * automatically add field values for all parent terms of the selected terms.
 */

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_entity_presave().
 *
 * Saves term lineage when entity is saved.
 */
function taxonomy_set_lineage_entity_presave($entity) {

  // We only handle content entities.
  if ($entity instanceof ContentEntityBase) {
    $settings = \Drupal::config('taxonomy_set_lineage.settings');
    $entity_type = $entity->getEntityTypeId();
    $vocabulary = $settings->get('vocabulary');

    // Check that we have a vocabulary selected in the settings.
    if (!empty($vocabulary)) {
      $lineage_fields = [];

      if (empty($settings->get('entities')) && empty($settings->get('bundles')) && empty($settings->get('fields'))) {
        // Only vocabulary selected so get entity reference fields.
        $lineage_fields = _taxonomy_set_lineage_get_fields($entity, $vocabulary);
      }
      else {
        // Entity Types: check if matches entity.
        if (!empty($settings->get('entities'))) {
          if (in_array($entity_type, $settings->get('entities'))) {
            $lineage_fields = _taxonomy_set_lineage_get_fields($entity, $vocabulary);
          }
        }

        // Bundles: check if it matches entity.
        if (!empty($settings->get('bundles'))) {
          $bundle_id = $entity->bundle();
          if (in_array($bundle_id, $settings->get('bundles'))) {
            $lineage_fields = _taxonomy_set_lineage_get_fields($entity, $vocabulary);
          }
        }

        // Fields: check if field exists for entity and vocabulary is active.
        if (!empty($settings->get('fields'))) {
          $bundle_id = $entity->bundle();
          foreach ($settings->get('fields') as $field_id) {
            list($field_entity_type, $field_entity_bundle, $field_name) = explode('.', $field_id);
            if ($field_entity_type == $entity_type && $field_entity_bundle == $bundle_id) {
              $instances = \Drupal::entityTypeManager()->getStorage('field_config')->loadByProperties([
                'field_name' => $field_name,
              ]);
              $instance = reset($instances);
              $instance_settings = $instance->get('settings');
              if (!empty($instance_settings['handler_settings']['target_bundles'])) {
                if (!empty(array_intersect($vocabulary, array_values($instance_settings['handler_settings']['target_bundles'])))) {
                  $lineage_fields[] = $field_name;
                }
              }
            }
          }
        }
      }

      // Go through list of lineage fields, check if changes were made to terms.
      foreach ($lineage_fields as $field) {
        $values = $entity->get($field)->getValue();
        $tids = (empty($values)) ? [] : array_column($values, 'target_id');

        $original_values = (empty($entity->original)) ? [] : $entity->original->get($field)->getValue();
        $original_tids = (empty($original_values)) ? [] : array_column($original_values, 'target_id');

        // Are there changes?
        asort($tids);
        asort($original_tids);
        if (count(array_diff($tids, $original_tids)) > 0 || count(array_diff($original_tids, $tids)) > 0) {
          // Copy variable as code will be adding values to $tids.
          $new_tids = $tids;
          foreach ($new_tids as $tid) {
            $parents = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadAllParents($tid);
            if (count($parents) > 1) {
              // Find delta of child TID.
              $delta = array_search($tid, array_column($values, 'target_id'));
              $new_values = [];

              // Reverse parents so we can add them in the correct order.
              foreach (array_reverse($parents, TRUE) as $parent) {
                if (!in_array($parent->id(), $tids)) {
                  $new_values[] = ['target_id' => $parent->id()];
                }
              }
              // Add new vaules preceding the existing item.
              array_splice($values, $delta, 0, $new_values);
              $entity->get($field)->setValue($values);
              $tids[] = $parent->id();
            }
          }
        }
      }
    }
  }
}

/**
 * Private helper function: Get a list of taxonomy fields from the entity.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity we are dealing with.
 * @param array $vocabulary
 *   Optional: List of vocabulary ids to limit fields to.
 *
 * @return array
 *   Array of field names that we should save the lineage for.
 */
function _taxonomy_set_lineage_get_fields(EntityInterface $entity, array $vocabulary = []) {
  $lineage_fields = [];

  $entity_type = $entity->getEntityTypeId();
  $bundle_id = $entity->bundle();
  $entity_field_manager = \Drupal::service('entity_field.manager');
  $fields = $entity_field_manager->getFieldDefinitions($entity_type, $bundle_id);

  foreach ($fields as $field) {
    if ($field->getType() === 'entity_reference') {

      // Get field config which contains the target entity type.
      $field_storage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->loadByProperties([
        'field_name' => $field->getName(),
        'type' => 'entity_reference',
        'settings' => [
          'target_type' => 'taxonomy_term',
        ],
      ]);

      if (!empty($field_storage)) {
        $settings = $field->get('settings');
        // Check field settings to see if vocabulary matches.
        if (!empty($settings['handler_settings']['target_bundles'])) {
          if (empty($vocabulary) || !empty(array_intersect($vocabulary, array_values($settings['handler_settings']['target_bundles'])))) {
            $lineage_fields[] = $field->getName();
          }
        }
      }
    }
  }
  return $lineage_fields;
}
