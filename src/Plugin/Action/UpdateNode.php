<?php

namespace Drupal\taxonomy_set_lineage\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Check nodes to ensure taxonomy lineage is set for all relevant fields.
 *
 * @Action(
 *   id = "taxonomy_set_lineage_update_node",
 *   label = @Translation("Update Taxonomy Term Parents"),
 *   type = "node",
 * )
 */
class UpdateNode extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a CancelUser object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $settings = $this->configFactory->get('taxonomy_set_lineage.settings');
    $entity_type = $entity->getEntityTypeId();
    $vocabulary = $settings->get('vocabulary');

    if (empty($settings->get('entities')) && empty($settings->get('bundles')) && empty($settings->get('fields'))) {
      // Only vocabulary selected so get entity reference fields.
      $lineage_fields = _taxonomy_set_lineage_get_fields($entity, $vocabulary);
    }
    else {
      // Entity Types: check if matches entity.
      if (!empty($settings->get('entities'))) {
        if (in_array($entity_type, $settings->get('entities'))) {
          $lineage_fields = _taxonomy_set_lineage_get_fields($entity, $vocabulary);
        }
      }

      // Bundles: check if it matches entity.
      if (!empty($settings->get('bundles'))) {
        $bundle_id = $entity->bundle();
        if (in_array($bundle_id, $settings->get('bundles'))) {
          $lineage_fields = _taxonomy_set_lineage_get_fields($entity, $vocabulary);
        }
      }

      // Fields: check if field exists for entity and vocabulary is active.
      if (!empty($settings->get('fields'))) {
        $bundle_id = $entity->bundle();
        foreach ($settings->get('fields') as $field_id) {
          list($field_entity_type, $field_entity_bundle, $field_name) = explode('.', $field_id);
          if ($field_entity_type == $entity_type && $field_entity_bundle == $bundle_id) {
            $instances = $this->entityTypeManager->getStorage('field_config')->loadByProperties([
              'field_name' => $field_name,
            ]);
            $instance = reset($instances);
            $instance_settings = $instance->get('settings');
            if (!empty($instance_settings['handler_settings']['target_bundles'])) {
              if (!empty(array_intersect($vocabulary, array_values($instance_settings['handler_settings']['target_bundles'])))) {
                $lineage_fields[] = $field_name;
              }
            }
          }
        }
      }
    }

    // Go through list of lineage fields, check if changes were made to terms.
    foreach ($lineage_fields as $field) {
      $values = $entity->get($field)->getValue();
      $tids = (empty($values)) ? [] : array_column($values, 'target_id');
      $new_tids = $tids;

      foreach ($new_tids as $tid) {
        $parents = $this->entityTypeManager->getStorage('taxonomy_term')->loadAllParents($tid);
        if (count($parents) > 1) {
          // Find delta of child TID.
          $delta = array_search($tid, array_column($values, 'target_id'));
          $new_values = [];

          // Reverse parents so we can add them in the correct order.
          foreach (array_reverse($parents, TRUE) as $parent) {
            if (!in_array($parent->id(), $tids)) {
              $new_values[] = ['target_id' => $parent->id()];
            }
          }
          // Add new vaules preceding the existing item.
          array_splice($values, $delta, 0, $new_values);
          $entity->get($field)->setValue($values);
          $tids[] = $parent->id();
        }
      }
    }
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function access($entity, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowedIfHasPermission($account, 'administer taxonomy');
    return $return_as_object ? $result : $result->isAllowed();
  }

}
