<?php

namespace Drupal\Tests\taxonomy_set_lineage\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Tests\taxonomy\Functional\TaxonomyTestBase;

/**
 * Test setup for the Taxonomy Set Lineage module.
 *
 * @group taxonomy_set_lineage
 */
abstract class TaxonomySetLineageTestBase extends TaxonomyTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'taxonomy_set_lineage',
    'field',
    'node',
    'taxonomy',
    'block',
    'views',
  ];

  /**
   * The taxonomy vocabulary.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $taxonomyVocabulary;

  /**
   * The taxonomy field.
   *
   * @var string
   */
  protected $taxonomyFieldName;

  /**
   * Default theme for testing.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Set to TRUE to strict check all configuration saved.
   *
   * @var bool
   */
  protected $strictConfigSchema = TRUE;

  /**
   * Performs the basic setup tasks.
   */
  public function setUp(): void {
    parent::setUp();

    // Create vocabulary.
    $this->taxonomyVocabulary = $this->createVocabulary();
    $vocab_id = $this->taxonomyVocabulary->id();

    // Create terms.
    $this->taxonomyFieldName = 'field_' . $vocab_id;
    $handler_settings = [
      'target_bundles' => [
        $vocab_id => $vocab_id,
      ],
      'auto_create' => TRUE,
    ];
    $this->createEntityReferenceField('node', 'article', $this->taxonomyFieldName, $vocab_id, 'taxonomy_term', 'default', $handler_settings, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    $display_repository->getFormDisplay('node', 'article')
      ->setComponent($this->taxonomyFieldName, [
        'type' => 'options_select',
      ])
      ->save();
    $display_repository->getViewDisplay('node', 'article')
      ->setComponent($this->taxonomyFieldName, [
        'type' => 'entity_reference_label',
      ])
      ->save();
  }

  /**
   * Add configuration with our vocabulary.
   */
  protected function setConfig() {
    $this->config('taxonomy_set_lineage.settings')
      ->set('vocabulary', [
        $this->taxonomyVocabulary->id(),
      ])
      ->save();
  }

}
