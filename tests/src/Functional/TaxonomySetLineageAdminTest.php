<?php

namespace Drupal\Tests\taxonomy_set_lineage\Functional;

/**
 * Test for the Taxonomy Set Lineage settings form.
 *
 * @group taxonomy_set_lineage
 */
class TaxonomySetLineageAdminTest extends TaxonomySetLineageTestBase {

  /**
   * Use admin theme for testing of admin forms.
   *
   * @var string
   */
  protected $defaultTheme = 'claro';

  /**
   * Tests the configuration page access.
   */
  public function testConfigPage() {
    $this->drupalGet('/admin/config/content/taxonomy_set_lineage');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->drupalCreateUser([
      'administer taxonomy',
    ]));

    $this->drupalGet('admin/config/content/taxonomy_set_lineage');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->elementExists('css', 'input[value="' . $this->taxonomyVocabulary->id() . '"]');
    $this->assertSession()->elementExists('css', 'input[value="node.article.' . $this->taxonomyFieldName . '"]');
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('edit-vocabulary-' . $this->taxonomyVocabulary->id(), $this->taxonomyVocabulary->id());
    $page->pressButton('Save configuration');
    $this->assertSession()->elementExists('css', 'input[value="' . $this->taxonomyVocabulary->id() . '"][checked=checked]');
    $config = $this->config('taxonomy_set_lineage.settings');
    $this->assertContains($this->taxonomyVocabulary->id(), $config->get('vocabulary'), 'Config does not taxonomy vocabulary');
  }

}
